import { 
    Button, 
    Container, 
    Form, 
    Table 
} from "react-bootstrap";
import { 
    getUsers 
} from "../services/users";
import { 
    useLoaderData,
    useSubmit,
    useNavigation,
    Form as RouterForm,
 } from "react-router-dom";
import { 
    CheckCircleFill, 
    PlusCircle, 
    PlusCircleFill,
    PlusSquare,
    PlusSquareFill,
    XCircleFill 
} from "react-bootstrap-icons";
import { useEffect } from "react";
import { LinkContainer } from "react-router-bootstrap";
import ScrollUsersTable from "../components/scrollUsersTable";

export async function loader({ request }) {
    const url = new URL(request.url);
    const q = url.searchParams.get("q");

    const users = await getUsers();
    if (!users) {
        throw new Response("", {
            status: 500,
            statusText: "Unexpected server error",
        })
    }
    console.log(users);
    let filteredUsers = [];
    if (q) {
        console.log(q);
        filteredUsers = users.filter(user => {
            return `${user.name} ${user.lastName1} ${user.lastName2}`.toLocaleLowerCase().includes(q.toLocaleLowerCase()) || 
            user.email.toLocaleLowerCase().includes(q.toLocaleLowerCase());
        });
        return { filteredUsers, q };
    }
    filteredUsers = [...users];
    return { filteredUsers, q };
}

export default function Users() {
    const { filteredUsers, q } = useLoaderData();
    const submit = useSubmit();
    const navigation = useNavigation();

    const searching = navigation.location &&
        new URLSearchParams(navigation.location.search).has("q");

    useEffect(() => {
        document.getElementById("q").value = q;
      }, [q]);

      const searchingClassName = searching ? 'loading' : '';

      const inputClassNames = `form-control w-25 ${searchingClassName}`;

    return (
        <>
            <Form id="search-form" role="search" className="position-relative col-8">
                <input
                    id="q"
                    className={inputClassNames}
                    aria-label="Search contacts"
                    placeholder="Search User"
                    type="search"
                    name="q"
                    defaultValue={q}
                    onChange={(event) => {
                        const isFirstSearch = q == null;
                        submit(event.currentTarget.form, {
                            replace: !isFirstSearch,
                        });
                    }}
                />

                <div 
                    className="spinner-border spinner-border-sm position-absolute top-50 start-1 translate-middle-y"
                    aria-hidden 
                    hidden={!searching} 
                />

                <div
                    className="sr-only"
                    aria-live="polite"
                ></div>
            </Form>

            <Container className="col-8 d-inline my-5">
                <LinkContainer to={'/users/create'}>
                    <Button variant="success" className="btn-lg">
                    <PlusSquareFill /> <span>Create User</span>
                    </Button>
                </LinkContainer>
            </Container>

            <ScrollUsersTable users={filteredUsers} />
        </>
    );
}