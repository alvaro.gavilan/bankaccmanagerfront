import { Container } from "react-bootstrap";
import { Form, redirect, useLoaderData, useNavigate } from "react-router-dom";
import { updateUser } from "../../services/users";

export async function action({ request, params }) {
    const formData = await request.formData();
    const updates = Object.fromEntries(formData);
    await updateUser(params.userId, updates);
    return redirect(`/users/${params.userId}`);
}

export default function EditUser() {
    const { user } = useLoaderData();
    const navigate = useNavigate();

    return (
        <Container className="row vh-100">  
                <Container className="text-center col-7 d-flex justify-content-center align-items-center">
                    <h1>Edit User</h1>
                </Container>
                <Container className="col-8 create-form">
                    <Form method="post" id="user-form" className="border rounded-4 p-5 h-100">
                        <div className="form-group row h-25">
                            <span className="col-3">Name</span>
                            <div className="col-3">
                                <input 
                                className="form-control form-control-lg w-100"
                                placeholder="First name"
                                aria-label="First name"
                                type="text" 
                                name="name"
                                defaultValue={user.name}
                                />
                            </div>
                            <div className="col-3">
                                <input 
                                className="form-control form-control-lg w-100"
                                placeholder="Last name"
                                aria-label="Last name"
                                type="text"
                                name="lastName1"
                                defaultValue={user.lastName1}
                                />
                            </div>
                            <div className="col-3">
                                <input 
                                className="form-control form-control-lg w-100"
                                placeholder="Second last name"
                                aria-label="Second last name"
                                type="text" 
                                name="lastName2"
                                defaultValue={user.lastName2}
                                />
                            </div>
                        </div>

                        <div className="form-group row h-25">
                            <span className="col-3">Email</span>
                            <div className="col-9">
                                <input 
                                className="form-control form-control-lg w-100"
                                placeholder="example@email.com"
                                aria-label="example@email.com"
                                type="text" 
                                name="email"
                                defaultValue={user.email}
                                />
                            </div>
                        </div>

                        <div className="form-group row h-25">
                            <span className="col-3">Phone Number</span>
                            <div className="col-9">
                                <input 
                                className="form-control form-control-lg w-100"
                                placeholder="XXX-XX-XX-XX"
                                aria-label="Phone"
                                type="tel" 
                                // pattern="[0-9]{3}-[0-9]{2}-[0-9]{2}-[0-9]{2}"
                                name="phone"
                                defaultValue={user.phone}
                                />
                            </div>
                        </div>

                        <div className="form-group row h-25 justify-content-around align-items-center">
                            <button type="submit" className="btn btn-lg btn-success col-4">
                                Save
                            </button>

                            <button 
                                className="btn btn-lg btn-danger col-4"
                                type="button"
                                onClick={() => {
                                    navigate(-1);
                                }}    
                            >
                                Cancel
                            </button>
                        </div>
                    </Form>
                </Container>
        </Container>
    );
}