import { useLoaderData } from "react-router-dom";
import { getUserById } from "../services/users";

export async function loader({ params }) {
    const user = await getUserById(1);
    if (!user) {
        throw new Response("", {
            status: 404,
            statusText: "User not found",
        })
    }
    return { user };
}

export default function Index() {
    const { user } = useLoaderData();

    return (
        <div>
            <h1 className="text-center">
                Bienvenido {`${user.name} ${user.lastName1} ${user.lastName2}`}
            </h1>
        </div>
    );
}