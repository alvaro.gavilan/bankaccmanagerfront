import { redirect } from "react-router-dom";
import { deleteUser } from "../../services/users";

export async function action({ params }) {
    const response = await deleteUser(params.userId);
    return redirect("/users");
}