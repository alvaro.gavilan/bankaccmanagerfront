import { Outlet } from "react-router-dom";
import Footer from "../components/footer";
import Header from "../components/header";

export default function Root() {
    return (
      <>
        <Header />
          <main className="h-100 m-5 row justify-content-center align-items-center">
          <Outlet />
          </main>
        <Footer />
      </>
    );
  }