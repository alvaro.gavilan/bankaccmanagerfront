import React from 'react'
import ReactDOM from 'react-dom/client'
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import './main.css'
import Root from './routes/root';
import ErrorPage from './error-page';
import User from './components/user';
import {
  loader as userLoader,
} from './components/user';
import {
  loader as indexLoader
} from './routes/index';
import {
  loader as usersLoader,
} from './routes/users';
import Index from './routes';
import Users from './routes/users';
import Clients from './routes/clients';
import Accounts from './routes/accounts';
import Currencies from './routes/currencies';
import Funds from './routes/funds';
import Transactions from './routes/transactions';
import CreateUser, { action as createUserAction } from './routes/creates/createUser';
import EditUser, { action as editUserAction } from './routes/edits/editUser';
import { action as disableUserAction } from './routes/destroys/destroyUser';

const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
    errorElement: <ErrorPage />,
    children: [
      {
        errorElement: <ErrorPage />,
        children: [
          {
            index: true,
            element: <Index />,
            loader: indexLoader,
          },
          {
            path: "users",
            element: <Users />,
            loader: usersLoader,
          },
          {
            path: "users/create",
            element: <CreateUser />,
            action: createUserAction,
          },
          {
            path: "users/:userId",
            element: <User />,
            loader: userLoader,
          },
          {
            path: "users/:userId/edit",
            element: <EditUser />,
            loader: userLoader,
            action: editUserAction,
          },
          {
            path: "users/:userId/disable",
            action: disableUserAction,
          },
          {
            path: "clients",
            element: <Clients />,

          },
          {
            path: "accounts",
            element: <Accounts />,

          },
          {
            path: "currencies",
            element: <Currencies />,

          },
          {
            path: "transactions",
            element: <Transactions />,

          },
          {
            path: "funds",
            element: <Funds />,

          },
        ]
      }
    ],
  },
]);

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
)
