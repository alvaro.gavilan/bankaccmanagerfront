import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';

const Footer = () => {
  return (
    <footer className="bg-body-tertiary text-light py-4">
      <Container>
        <Row>
          <Col>
            <h5 className="text-primary">Enlaces</h5>
            <ul className="list-unstyled">
              <li><a href="#" className="text-light">Inicio</a></li>
              <li><a href="#" className="text-light">Servicios</a></li>
              <li><a href="#" className="text-light">Contacto</a></li>
            </ul>
          </Col>
          <Col>
            <h5 className="text-primary">Acerca de Nosotros</h5>
            <ul className="list-unstyled">
              <li><a href="#" className="text-light">Equipo</a></li>
              <li><a href="#" className="text-light">Historia</a></li>
              <li><a href="#" className="text-light">Misión</a></li>
            </ul>
          </Col>
          <Col>
            <h5 className="text-primary">Contacto</h5>
            <ul className="list-unstyled">
              <li><a href="#" className="text-light">Correo</a></li>
              <li><a href="#" className="text-light">Teléfono</a></li>
              <li><a href="#" className="text-light">Ubicación</a></li>
            </ul>
          </Col>
        </Row>
        <Row className="mt-3">
          <Col>
            <p className="text-muted">&copy; 2023 Nombre de la Compañía. Todos los derechos reservados.</p>
          </Col>
        </Row>
      </Container>
    </footer>
  );
};

export default Footer;
