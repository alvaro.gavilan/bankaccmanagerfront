import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import {LinkContainer} from 'react-router-bootstrap'

function Header() {
  return (
    <Navbar expand="lg" className="bg-body-tertiary">
      <Container>
        <LinkContainer to={'/'}>
          <Navbar.Brand>
            Bank Account Manager  
          </Navbar.Brand>
        </LinkContainer>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <LinkContainer to={'/users'}>
              <Nav.Link>Users</Nav.Link>
            </LinkContainer>
            
            <LinkContainer to={'/clients'}>
              <Nav.Link>Clients</Nav.Link>
            </LinkContainer>

            <LinkContainer to={'/accounts'}>
              <Nav.Link>Accounts</Nav.Link>
            </LinkContainer>

            <LinkContainer to={'/currencies'}>
              <Nav.Link>Currenncies</Nav.Link>
            </LinkContainer>

            <LinkContainer to={'/transactions'}>
              <Nav.Link>Transactions</Nav.Link>
            </LinkContainer>

            <LinkContainer to={'/funds'}>
              <Nav.Link>Funds</Nav.Link>
            </LinkContainer>
            
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default Header;