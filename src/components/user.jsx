import { Form, useLoaderData } from "react-router-dom";
import { getUserById } from "../services/users";
import { Button, Container } from "react-bootstrap";
import { CheckCircle, Envelope, PencilSquare, PersonCircle, Telephone, TelephoneFill, XCircle, XCircleFill } from "react-bootstrap-icons";
import { LinkContainer } from "react-router-bootstrap";
import { useEffect, useState } from "react";

export async function loader({ params }) {
    const user = await getUserById(params.userId);
    if (!user) {
        throw new Response("", {
            status: 404,
            statusText: "User not found",
        })
    }
    console.log(user);
    return { user };
}

// id, name lastName1, lastName2, email, phone, active

export default function User() {
    const { user } = useLoaderData();
    const [enableForm, setEnableForm] = useState();
    
    useEffect(() => {
        setEnableForm(
            user.active ? 
                <Form
                    className="ms-auto"
                    method="post" 
                    action="disable"
                    onSubmit={(event) => {
                    if (!confirm("Please confirm you want to disable this user.")) {
                        event.preventDefault();
                    }
                    }}
                >
                    <button className="btn btn-lg btn-danger" type="submit"><XCircle /> Disable</button>
                </Form>
            :
                <Form
                    className="ms-auto"
                    method="post"
                    action="enable"
                    onSubmit={(event) => {
                    if (!confirm("Please confirm you want to enable this user.")) {
                        event.preventDefault();
                    }
                    }}
            >
                <button className="btn btn-lg btn-success"><CheckCircle /> Enable</button>
            </Form>

        );
    }, []);

    return (
        <Container className="vh-100 bg-secondary">

            <Container className="m5 vh-100 border border-danger m-auto">

                <Container className="row bg-success h-25 align-items-center">
                    <Container className="col-4">
                            <PersonCircle size={300} />
                    </Container>

                    <Container className="col-8">
                        <h1>{`${user.name} ${user.lastName1} ${user.lastName2}`}</h1>
                        <h4><Envelope />{' - '}<span className="text-primary">{` ${user.email}`}</span></h4>
                        <h5><Telephone />{` - ${user.phone}`}</h5>

                        <Container className="p-0 d-flex w-30 m-0 justify-content-around">
                            <Form action="edit">
                                <button className="btn btn-lg btn-warning" type="submit"><PencilSquare size={20} /> Edit</button>
                            </Form>
                        
                            {enableForm}
                        </Container>
                        
                    </Container>

                </Container>

                <Container className="row bg-warning h-75">
                    <div className="border border-info h-90 mt-big">
                        <div className="bg-info h-100">
                            tablita wapa
                        </div>
                    </div>
                </Container>

            </Container>

        </Container>
    );
}