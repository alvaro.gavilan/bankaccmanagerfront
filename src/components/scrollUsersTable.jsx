import { Table } from "react-bootstrap";
import { CheckCircleFill, XCircleFill } from "react-bootstrap-icons";
import { LinkContainer } from "react-router-bootstrap";

export default function scrollUsersTable({ users }) {
    return (
        <div className="custom-table-height custom-table-col overflow-auto">
                <Table className="table-striped table-hover">
                    <thead className="sticky-top">
                        <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone Number</th>
                        <th className="text-center">Active</th>
                        </tr>
                    </thead>
                    <tbody>
                        {users.map((user, i) => {
                            return (
                                <LinkContainer to={`/users/${user.id}`} key={user.id}>
                                    <tr>
                                        <td>{i + 1}</td>
                                        <td>{`${user.name} ${user.lastName1} ${user.lastName2}`}</td>
                                        <td>{user.email}</td>
                                        <td>{user.phone}</td>
                                        <td className="text-center">
                                            {
                                                user.active ? 
                                                    <CheckCircleFill color="green" size={23}/> : 
                                                    <XCircleFill color="red" size={23}/>
                                            }
                                        </td>
                                    </tr>
                                </LinkContainer>
                            );
                        })}
                    </tbody>
                </Table>
            </div>
    );
}