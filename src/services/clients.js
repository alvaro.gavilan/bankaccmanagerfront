const clientsURL = 'http://localhost:8091/clients';

const requestOptions = {
    headers: {
        'Content-Type': 'application/json; charset=utf-8',
    }
}

export async function getClients() {
    const response = await fetch(`${clientsURL}/all`, { ...requestOptions, method: 'GET' });
    if (response.ok) {
        return await response.json();
    }
    const errorMessage = await response.text();
    throw new Error(errorMessage);
}

export async function getClientById(id) {
    const response = await fetch(`${clientsURL}/id/${id}`, { ...requestOptions, method: 'GET' });
    if (response.ok) {
        return await response.json(); 
    }
    const errorMessage = await response.text();
    throw new Error(errorMessage);
}

export async function createClient(request) {
    const response = await fetch(`${clientsURL}/add`, { 
        ...requestOptions, 
        method: 'POST', 
        body: JSON.stringify(request) 
    });
    const status = response.status;
    const statusText = await response.text();
    return { status, statusText };
}

export async function updateClient(clientId, updates) {
    const response = await fetch(`${clientsURL}/update/${clientId}`, {
        ...requestOptions,
        method: 'PUT',
        body: JSON.stringify(updates),
    });
    const status = response.status;
    const statusText = await response.text();
    return { status, statusText };
}

const newClient = {
    name: 'Raquel',
    lastName1: 'Gavilan',
    lastName2: 'Fernandez-Contero',
    email: 'raquelgfdezc@gmail.com',
    phone: '916370159',
    identifier: '54023884G'
}


const response = await updateClient(12, newClient);

console.log(response);