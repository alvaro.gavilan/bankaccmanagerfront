const usersURL = 'http://localhost:8090/users';
                 
const getRequestOptions = {
    method: 'GET',
    mode: 'no-cors',
    headers: {
        'Content-Type': 'application/json',
    },
};

export async function getUserById(id) {
    const response = await fetch(`${usersURL}/id/${id}`, getRequestOptions);
    if (response.ok) {
        return await response.json(); 
    }
    const errorMessage = await response.text();
    throw new Error(errorMessage);
}

export async function getUsers() {
    const response = await fetch(`${usersURL}/all`, getRequestOptions);
    if (response.ok) {
        return await response.json(); 
    }
    const errorMessage = await response.text();
    throw new Error(errorMessage);
}

export async function createUser(request) {
    const response = await fetch(`${usersURL}/add`, { 
        headers: { "Content-Type": "application/json; charset=utf-8" },
        method: 'POST',
        body: JSON.stringify(request)
    });
    const status = response.status;
    const statusText = await response.text();
    return { status, statusText };
}

export async function updateUser(userId, updates) {
    const response = await fetch(`${usersURL}/update/${userId}`, {
        headers: { "Content-Type": "application/json; charset=utf-8" },
        method: 'PUT',
        body: JSON.stringify(updates),
    });
    const status = response.status;
    const statusText = await response.text();
    return { status, statusText };
}

export async function deleteUser(id) {
    const response = await fetch(`${usersURL}/delete/${id}`, {
        headers: { "Content-Type": "application/json; charset=utf-8" },
        method: 'DELETE',
    });
    const status = response.status;
    const statusText = await response.text();
    return { status, statusText };
}